﻿using System.Web;
using System.Web.Mvc;
using NCD.Application.Domain;
using NCD.Application.Services;
using NCD.Models;
using Ninject;
using System.Linq;

namespace NCD.Controllers
{
    public class CriminalController : Controller
    {
        [Inject]
        public ISearchService SearchService { get; set; }

        [Inject]
        public IEmailService EmailService { get; set; }

        public ActionResult Index()
        {
            if (this.Request.IsAuthenticated)
            {
                var model = new SearchViewModel
                {
                    Email = this.HttpContext.GetOwinContext().Request.User.Identity.Name,
                };

                return View(model);
            }
            return RedirectToAction("Index", "");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Search(SearchViewModel model)
        {
            if (ModelState.IsValid)
            {
                var searchRequest = new SearchRequest
                {
                    Email = model.Email,
                    MaxNumberResults = model.MaxNumberResults.Value,
                    Name = model.Name,
                    Sex = model.Sex,
                    AgeFrom = model.AgeFrom,
                    AgeTo = model.AgeTo,
                    HeightTo = model.HeightTo,
                    HeightFrom = model.HeightFrom,
                    WeightFrom = model.WeightFrom,
                    WeightTo = model.WeightTo
                };

                var criminals = SearchService.SearchCriminal(searchRequest);
                EmailService.Send(searchRequest.Email, criminals);

                if (!criminals.Any())
                {
                    return View("Negate");
                }
                
                    return View("Confirmation");
                
            }

            return View("Index", model);
        }
    }
}
