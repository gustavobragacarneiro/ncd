﻿using System.ComponentModel.DataAnnotations;

namespace NCD.Models
{
    public class SearchViewModel
    {

        //[Required, RegularExpression(@"^[0-9]+$", ErrorMessage = "Type just numbers.")]
        //[Range(1, 50, ErrorMessage = "{0} must be between 1 and 50.")]
        [Required(ErrorMessage = "You must enter a number between 1 and 50")]
        //[Display(Order = 1, Name = "MaxNumberResults")]
        [Range(1, 50, ErrorMessage = "{0} must be between 1 and 50.")]
        //[RegularExpression(@"^[0-9]+$", ErrorMessage = "Type just numbers.")]
        public int? MaxNumberResults { get; set; }

        [RegularExpression(@"^[a-zA-Z''-'\s]{1,40}$",
         ErrorMessage = "Characters are not allowed.")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        public string Name { get; set; }

        [Required]
        public string Sex { get; set; }

        [RegularExpression(@"^(0?[0-9]{1,2}|1[0-7][0-9]|100)$", ErrorMessage = "Type just numbers.")]
        [Range(12, 100, ErrorMessage = "{0} must be between 12 and 100.")]
        public int? AgeFrom { get; set; }

        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Type just numbers.")]
        [Range(12, 100, ErrorMessage = "{0} must be between 12 and 100.")]
        public int? AgeTo { get; set; }
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Type just numbers.")]
        [Range(50, 250, ErrorMessage = "{0} must be between 50 and 250.")]
        public double? HeightFrom { get; set; }
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Type just numbers.")]
        [Range(50, 250, ErrorMessage = "{0} must be between 50 and 250.")]
        public double? HeightTo { get; set; }
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Type just numbers.")]
        [Range(50, 250, ErrorMessage = "{0} must be between 50 and 250.")]
        public double? WeightFrom { get; set; }
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Type just numbers.")]
        [Range(50, 250, ErrorMessage = "{0} must be between 50 and 250.")]
        public double? WeightTo { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(250, ErrorMessage = "The email is to long, it should not exced 250 characters.")]
        public string Email { get; set; }
        
    }
}