﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using iTextSharp.text;
using iTextSharp.text.pdf;
using NCD.Application.Domain;
using NCD.Application.Services;
using RazorEngine.Templating;
using System.Net;

namespace NCD.Infrastructure
{
    public class EmailService : IEmailService
    {
        public void Send(string emailAddress, IEnumerable<Person> persons)
        { 
            if (!string.IsNullOrWhiteSpace(emailAddress))
            {
                if (persons != null && persons.Any())
                {
                    var pdfs = new List<ReportFile>();

                    foreach (var person in persons)
                    {
                        var htmlTemplate = GenerateHtmlString(person);
                        var pdf = ConvertToPdf(htmlTemplate);
                        pdfs.Add(new ReportFile 
                        {
                            Name = person.Name,
                            Data = pdf
                        });
                    }
                    SendEmail(emailAddress, pdfs);
                }
            }            
        }

        private static void SendEmail(string emailAddress, List<ReportFile> pdfs)
        {
            var client = new SmtpClient();
            //NetworkCredential cred = new NetworkCredential("gustavo.carneiro@topdown.com.br", "$0un4pst3r");
            //SmtpClient mailClient = new SmtpClient();
            //mailClient.EnableSsl = true;
            ////mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            //mailClient.UseDefaultCredentials = false;
            ////mailClient.Timeout = 20000;
            ////mailClient.Credentials = cred;

            var batches = (pdfs.Count / 10) + 1;

            for (var i = 0; i <= batches - 1; i++)
            {
                var subject = "Criminal Profiles";

                if (batches > 1)
                {
                    subject += string.Format(" - Part {0}/{1}", (i + 1), batches);
                }

                using (var message = new MailMessage())
                {
                    message.To.Add(new MailAddress(emailAddress));
                    message.From = new MailAddress(@"gustavo.carneiro@topdown.com.br");
                    message.Subject = subject;
                    message.Body = "Hi, we are sending you the results of your search. Please open the attached files.";
                    message.IsBodyHtml = false;

                    foreach (var pdf in pdfs.Skip(i * 10).Take(10))
                    {
                        MemoryStream stream = new MemoryStream(pdf.Data);
                        Attachment attachment = new Attachment(stream, pdf.Name, "application/pdf");
                        message.Attachments.Add(attachment);
                    }

                    client.Send(message);
                }
            }
        }

        //       private static void SendEmail(string emailAddress, List<ReportFile> pdfs)
        //       {
        //           MailAddress fromAddress = new MailAddress("alfa@gmail.com", "Hendavane");
        //           MailAddress toAddress = new MailAddress("beta@gmail.com", "Hendavane");
        //           const string fromPassword = "alfaalfa";
        //           string subject = System.Environment.MachineName;
        //           string body = mypage;

        //           SmtpClient smtp = new SmtpClient()
        //           {
        //               Host = "smtp.gmail.com",
        //               Port = 587,
        //               EnableSsl = true,
        //               DeliveryMethod = SmtpDeliveryMethod.Network,
        //               Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
        //               Timeout = 20000
        //           };

        //           using (MailMessage message = new MailMessage(fromAddress, toAddress)
        //           {
        //               Subject = subject,
        //               IsBodyHtml = true,
        //               Body = body
        //           };)
        //{
        //               // Create the file attachment for this e-mail message.
        //               Attachment data = new Attachment(file, MediaTypeNames.Application.Octet);

        //               // Add time stamp information for the file.
        //               ContentDisposition disposition = data.ContentDisposition;
        //               disposition.CreationDate = System.IO.File.GetCreationTime(file);
        //               disposition.ModificationDate = System.IO.File.GetLastWriteTime(file);
        //               disposition.ReadDate = System.IO.File.GetLastAccessTime(file);

        //               // Add the file attachment to this e-mail message.
        //               message.Attachments.Add(data);

        //               smtp.Send(message);
        //           }

        //       }

        private static string GenerateHtmlString(Person person)
        {
            if (person != null)
            {
                var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\Services\EmailTemplate.cshtml");
                var templateService = new TemplateService();
                var template = File.ReadAllText(path);
                var htmlString = templateService.Parse(template, person, null, person.Id.ToString());

                return htmlString;
            }

            return null;
        }

        private static byte[] ConvertToPdf(string htmlTemplate)
        {
            try
            {
                byte[] bytes;
                using (var ms = new MemoryStream())
                {
                    using (var doc = new Document())
                    {
                        using (var writer = PdfWriter.GetInstance(doc, ms))
                        {
                            doc.Open();
                            using (var htmlWorker = new iTextSharp.text.html.simpleparser.HTMLWorker(doc))
                            {
                                using (var sr = new StringReader(htmlTemplate))
                                {
                                    htmlWorker.Parse(sr);
                                }
                            }
                            doc.Close();
                        }
                    }
                    bytes = ms.ToArray();
                }
                return bytes;
            }
            catch { }

            return null;
        }        
    }
}
